<?php


class TasksController  extends Controller {

	public function getAllTasks(){

		$tasks = TaskDetails::all();
		$result_tasks = [];
		foreach ($tasks as $task) {
			$task->task_status = ($task->task_status=='false') ? false :true;
			$result_tasks[] = $task;
		}
		return json_encode($tasks);
		

	}

	public function addTask()
	{
		$request = Request::all();
		$task = new TaskDetails;
		$task->task_status = 'false';
		$task->task_name = $request['title'];

		$task->save();
	}

	public function update()
	{
		$request = Request::all();
		$task = TaskDetails::find($request['task_id']);

		$task->task_status = ($request['task_status']=='false')? 'true' : 'false';
		$task->save();
	}

	public function destroy($task_id)
	{	
		$task = TaskDetails::find($task_id);
	    TaskDetails::where('id', '=', $task_id)->delete();
	    return json_encode([$task]);

	}

	public function show($task_id)
	{	

	     $task = TaskDetails::find($task_id);
	      return json_encode([$task]);
	}



}
