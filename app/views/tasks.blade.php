<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tasks</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.sidr/2.2.1/stylesheets/jquery.sidr.dark.min.css">

</head>
<body>
<br />
<br />
<br />
<br />

<style type="text/css">

</style>

<!-- header -->
<div class="row">

	<div class="col-md-4 col-md-offset-2">
		<table id='taskDetailsTable' class="table table-bordered " style="position: absolute;top: 50%;left: 50%;">
			<thead>
				<tr style="background: yellow;">
					<td>משימות</td>
					<td><button  id='addTask' ></button></td>
				</tr>
			</thead>
			<tfoot>
		    <tr>
		      <td><p>לסיכום:{{$new_tasks}} הושלמו:{{$done_tasks}} סה״כ:{{$total_tasks}}</p></td>
		    </tr>
		  </tfoot>
			<tbody id="taskDetailsTableBody">
				@foreach($tasks as $task)
				<tr id="{{ $task->task_id }}">
				@if($task->task_status=='done')
					<td><input type="checkbox" class="task_status"></input><span style="text-decoration: line-through;">{{ $task->task_id }}. {{ $task->task_name }} </span></td>
				@else
					<td><input type="checkbox" class="task_status"></input><span style="text-decoration: none;">{{ $task->task_id }}. {{ $task->task_name }} </span></td> 		
				@endif					
				<td><a href="javascript:deleteTask('{{ $task->task_id }}');">Delete</a></td>
				</tr>
				@endforeach

			</tbody>
		</table>	
	</div>

</div>


<script src="//cdn.jsdelivr.net/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $("#addTask").click(function(){
        $.ajax({
		   method: "get",
		   url: "/addTask",
			success:function(){
					window.location = '/';
				//console.log("G");
			}
		});
    });



    $('input:checkbox').on('change', function () {
        var input = $(this).next('span');
        var task_id = $(this).closest('tr').attr('id');
        var task_status ="";
        if($(this).is(":checked")){
            $(input).css('textDecoration', 'line-through');
             task_status = 'done';
        } else {
            $(input).css('textDecoration', 'none');
             task_status = 'new';
        }


		$.ajax({
            url: '/update',
            type:'POST',
            dataType:'JSON',
            data:{
                "task_id" : task_id , "task_status" : task_status
            },
            success:function(){
                
            }
        });
    });


    
});

function deleteTask(task_id) {
	        $.ajax({
	            type: "DELETE",
	            url: '/task/' + task_id, //resource
	            success: function(affectedRows) {
	                //if something was deleted, we redirect the user to the users page, and automatically the user that he deleted will disappear
	                if (affectedRows > 0) window.location = '/';
	            }
	        });
	}
</script>

</body>
</html>