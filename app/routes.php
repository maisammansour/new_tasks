<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::get('/', 'HomeController@showWelcome');
Route::get('/tasks/{task_id}', 'TasksController@show');
Route::get('/tasks', 'TasksController@getAllTasks');
Route::get('/addTask', 'TasksController@addTask');
Route::post('/update', 'TasksController@update');

Route::delete('/task/{task_id}', 'TasksController@destroy');
